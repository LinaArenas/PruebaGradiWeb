# GradiWeather 

## Prueba Web App con servicios REST 
Proyecto creado para consumir el API Openweathermap, prueba de ingreso para Gradiweb

### [lmarenasa-gradiweb.netlify.app](https://lmarenasa-gradiweb.netlify.app/#/)


### _Nota:_
En el despliegue, en Netlify, no se podrá visualizar la sección del **Forecast**, debibo a que esta sección hace uso de un **_servidor FAKE_**, para la simulación de servicio en desarrollo.

Esta opción se tomó debido a que en **_openweathermap_**, esa sección es de pago.

## Tecnologías y herramientas utilizadas

* [VueJs](https://vuejs.org/) - El framework web usado Frontend JS
* [Vuetify](https://vuetifyjs.com/en/) - El framework web usado Style
* [Sass](https://sass-lang.com/) - Hoja de estilos en cascada
* [Json-Server](https://github.com/typicode/json-server) - API REST de prueba para desarrollo
* [npm-run-all](https://www.npmjs.com/package/npm-run-all)


### Ejecución proyecto 
```
$ npm install 
```
```
$ npm run start
```


![](https://gitlab.com/LinaArenas/PruebaGradiWeb/-/raw/master/src/assets/resultado.png)
> Resultado final

| Nombre  | Correo  |
| ------------ | ------------ |
|  Lina María Arenas Aguirre | lmarenasa@outlook.com  |