import Convert from "../mixins/convert"
export default {
  data(){
    return{
      apykey: "c3cb975f0f4de35d7b934497a789fe58",
      respuesta: [],
    }
  },
  mixins: [Convert],
  methods: {
    // obteniendo los datos 
    getClima() {
      const uri = `https://api.openweathermap.org/data/2.5/weather?q=${this.city},${this.countrycode}&appid=${this.apykey}`;
      this.$http.get(uri).then(
        response => {
          //console.log(response.data.weather)
          this.todos = response.data;
          this.currentTemp = response.data.main.temp;
          this.countrycode = response.data.sys.country;
          this.pressure = response.data.main.pressure;
          this.overcast = response.data.weather[0].description;
          this.icon = response.data.weather[0].icon;
          this.tempgrados = this.convertKelvinToCelsius(this.currentTemp);
          // console.log("celsius bogota")
          this.sunrise = new Date(response.data.sys.sunrise * 1000)
            .toLocaleTimeString("en-GB")
            .slice(0, 4);
          this.sunset = new Date(response.data.sys.sunset * 1000)
            .toLocaleTimeString("en-GB")
            .slice(0, 4);
          //console.log('Normal todo ' ,this.todos);
          //console.log('Normal todo ' ,this.tempgrados);
        },
        error => {
          console.error(error);
        }
      );
    },
    getWeather(country, city) {
      const uri = `https://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${this.apykey}`;
      this.$http.get(uri).then(
        response => {
          response = response.data;
          this.respuesta.push((response))  
        },
        error => {
          console.error(error);
        }
      );
    },
  }
}